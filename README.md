# CI for osbuild-composer project

This repository essentially is a mirror of https://github.com/osbuild/osbuild-composer
There are pipeline schedules (here in gitlab.com) which pull github content daily and
run tests. See `Build -> pipeline schedules` and branches like `nightly-testing`.

For each PR on github a _new branch_ `PR-*` is created here to run CI, see trigger here:
https://github.com/osbuild/osbuild-composer/blob/main/.github/workflows/trigger-gitlab.yml
